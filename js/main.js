/*
 * main.js
 * Entry point for PLEditor
 * Created on 2/18/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//imports
const {app, BrowserWindow } = require('electron');
const path = require('path');

//keep a global reference to the window
let win;

//This function creates a window
function createWindow() {
	//create the window
	win = new BrowserWindow({width: 800, height: 600,
					resizable: false,
					fullscreenable: false,
					icon: 
				path.resolve('assets/icons/png/64x64.png'),
					webPreferences: {
						nodeIntegration: true
					}});

	//load the index HTML file
	win.loadFile('index.html');

	//add an event handler for closing the window
	win.on('closed', () => {
		//dereference the window object
		win = null;
	});

}

//add an event handler for starting the app
app.on('ready', createWindow);

//add an event handler for quitting the app when all windows are closed
app.on('window-all-closed', () => {
	//On macOS applications stay active until Cmd+Q is pressed
	if(process.platform !== 'darwin') {
		app.quit();
	}
});

//add a window activation event handler
app.on('activate', () => {
	//On macOS it's common to recreate a window when
	//the dock icon is clicked and there are no windows open
	if(win === null) {
		createWindow();
	}
});

//end of file
