/*
 * promptForValue.js
 * Defines a function that prompts for a plist value from the user
 * Created on 2/27/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//import
const showInputDialog = require('./showInputDialog.js').showInputDialog;

//prompts for a value and returns the input value
function promptForValue(curValue = null) {
	//prompt for the value
	return showInputDialog('Value Entry', 'Value: ', curValue);
}

//export
exports.promptForValue = promptForValue;

//end of file
