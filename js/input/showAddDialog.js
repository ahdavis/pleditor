/*
 * showAddDialog.js
 * Defines a function that shows a list addition type dialog
 * Created on 2/25/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
const { dialog } = require('electron').remote;
const AddDialogValues = require('./AddDialogValues.js');

//shows a list addition dialog box
//returns an AddDialogValue constant containing the selected option
function showAddDialog() {
	//define an array of button texts
	var buttonTexts = [ 'Dictionary', 
				'Array', 
				'String', 
				'Number', 'Cancel' ];

	//add a Boolean option if the app is running on macOS
	if(process.platform === "darwin") {
		buttonTexts.push('Boolean');
	}

	//define an object that holds the options for the dialog box
	var options = {
		type: "question",
		buttons: buttonTexts, 
		title: "Notice",
		message: 'Which type of list element do you wish to add?',
		cancelId: 4
	};

	//show the dialog and get the index of the clicked button
	var idx = dialog.showMessageBox(options);

	//get the clicked button text
	var clickText = buttonTexts[idx];

	//and return the proper value for the clicked button
	if(clickText === 'Dictionary') {
		return AddDialogValues.DICT;
	} else if(clickText === 'Array') {
		return AddDialogValues.ARRAY;
	} else if(clickText === 'String') {
		return AddDialogValues.STRING;
	} else if(clickText === 'Number') {
		return AddDialogValues.NUMBER;
	} else if(clickText === 'Boolean') {
		return AddDialogValues.BOOLEAN;
	} else {
		return AddDialogValues.CANCEL;
	}
}

//export
exports.showAddDialog = showAddDialog;

//end of file
