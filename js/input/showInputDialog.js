/*
 * showInputDialog.js
 * Defines a function that shows an input dialog
 * Created on 2/21/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//import
const prompt = require('electron-prompt');

//prompts for text input and returns the input text
function showInputDialog(boxTitle, boxLabel, defaulValue = null) {
	//define an object that holds options
	var options = {
		title: boxTitle,
		label: boxLabel,
		value: defaulValue,
		inputAttrs: {
			type: 'text',
			required: true
		}
	};

	//display the prompt
	return prompt(options);
}

//export
exports.showInputDialog = showInputDialog;

//end of file
