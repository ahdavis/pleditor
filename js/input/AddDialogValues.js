/*
 * AddDialogValues.js
 * Defines constants for return values of the showAddDialog function
 * Created on 2/25/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//no imports

//constant definitions
const DICT = 0;
const ARRAY = 1;
const STRING = 2;
const NUMBER = 3;
const BOOLEAN = 4;
const CANCEL = 5;

//exports
exports.DICT = DICT;
exports.ARRAY = ARRAY;
exports.STRING = STRING;
exports.NUMBER = NUMBER;
exports.BOOLEAN = BOOLEAN;
exports.CANCEL = CANCEL;

//end of file
