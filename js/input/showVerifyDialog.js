/*
 * showVerifyDialog.js
 * Defines a function that shows a verification dialog
 * Created on 2/21/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//import
const { dialog } = require('electron').remote;

//shows a verification dialog box
//returns true if "OK" is selected and false if "Cancel" is selected
function showVerifyDialog(messageText) {
	//define an array of button texts
	var buttonTexts = [ 'OK', 'Cancel' ];

	//define an object that holds the options for the dialog box
	var options = {
		type: "question",
		buttons: buttonTexts, 
		title: "Notice",
		message: messageText,
		cancelId: 1
	};

	//show the dialog and get the index of the clicked button
	var idx = dialog.showMessageBox(options);

	//and determine which button was clicked
	if(buttonTexts[idx] === 'OK') {
		return true;
	} else {
		return false;
	}
}

//export
exports.showVerifyDialog = showVerifyDialog;

//end of file
