/*
 * isalpha.js
 * Defines a function that returns whether a character is a letter
 * Created on 2/20/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//no imports

//function definition
function isalpha(ch) {
	//calculate whether ch is lowercase
	var isLower = ((ch >= 'a') && (ch <= 'z'));
	
	//calculate whether ch is uppercase
	var isUpper = ((ch >= 'A') && (ch <= 'Z'));

	//and return the logical OR of the two results
	return isLower || isUpper;
}

//export
exports.isalpha = isalpha;

//end of file
