/*
 * openPList.js
 * Defines a function that opens a plist file and returns its path
 * Created on 2/20/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//import
const { dialog } = require('electron').remote;

//function definition
function openPList() {
	//define dialog options
	var options = {
		title: 'Open Property List',
		openFile: true,
		openDirectory: false,
		multiSelections: false,
		filters: [
			{ name: 'Property Lists', extensions: ['plist']}
		]
	};

	//show the file open dialog and get an array of selected files
	//due to the multiSelections property being false,
	//the array should only have one element
	var files = dialog.showOpenDialog(options);

	//and return the first path in the array if the file was opened
	if(files !== undefined) {
		return files[0];
	} else {
		return null;
	}
}

//export the function
exports.openPList = openPList;

//end of file
