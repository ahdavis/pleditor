/*
 * savePList.js
 * Defines a function that saves a plist to a file
 * Created on 2/21/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
const { dialog } = require('electron').remote;
const PDict = require('../data/PDict.js').PDict;

//function definition
function savePList(dict) {
	//define options for the save dialog
	var options = {
		title: 'Save Property List',
		filters: [
			{ name: 'Property Lists', extensions: ['plist'] }
		]
	};

	//show the save dialog and get the path from it
	var path = dialog.showSaveDialog(options);
	
	//write the dictionary to the path
	if((dict instanceof PDict) && (path !== undefined)) {
		dict.writeToFile(path);
	}

	//and return the path
	return path;
}

//export
exports.savePList = savePList;

//end of file
