/*
 * index.js
 * Renderer process code for index.html
 * Created on 2/19/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
const constants = require('../util/constants.js');
const openPList = require('../util/openPList.js').openPList;
const savePList = require('../util/savePList.js').savePList;
const PDict = require('../data/PDict.js').PDict;
const PArray = require('../data/PArray.js').PArray;
const showVerifyDialog = require('../input/showVerifyDialog.js')
					.showVerifyDialog;
const showInputDialog = require('../input/showInputDialog.js')
					.showInputDialog;
const UserSession = require('../user/UserSession.js').UserSession;
const refresh = require('./refresh.js').refresh;
const assignMember = require('../data/assignMember.js').assignMember;
const pathToElement = require('../data/pathToElement.js').pathToElement;
const objForID = require('../data/objForID.js').objForID;
const promptForValue = require('../input/promptForValue.js')
				.promptForValue;
const promptForKey = require('../input/promptForKey.js').promptForKey;
const showAddDialog = require('../input/showAddDialog.js').showAddDialog;
const AddDialogValues = require('../input/AddDialogValues.js');

//get the div that holds the root of the plist
var div = document.getElementsByTagName("DIV")[constants.rootDivIdx];

//and set its id field
div.id = constants.rootDivID;

//create a user session object
var session = new UserSession();

//add an event handler for the app being ready
$(document).ready(function() {
	refresh(session);
});

//add an event handler for the open button
$("#openButton").click(function() {
	//make sure that the user has saved their work
	var shouldOpen = true;
	if(!session.saved) {
		if(!showVerifyDialog('You have not saved your work. ' +
					'Continue?')) {
			shouldOpen = false;
		}
	}
	
	if(shouldOpen) {
		session.path = openPList();
	}
	if(session.path) {
		session.dict.loadFromFile(session.path);
		refresh(session);
	}
});

//add an event handler for the save as button
$("#saveAsButton").click(function() {

	//save the plist
	var savePath = savePList(session.dict);

	//and set the saved flag
	if(savePath !== undefined) {
		session.path = savePath;
		session.saved = true;
		refresh(session);
	}
});

//add an event handler for the save button
$("#saveButton").click(function() {
	//only save if the path and dictionary are defined
	if(session.path && session.dict) {
		//write the current dictionary to a file
		session.dict.writeToFile(session.path);

		//and set the saved flag
		session.saved = true;
	} else {
		alert('The current list does not refer to a known ' +
			'property list file. Click "Save As" to ' +
			'save the current list to a file.');
	}
});

//add an event handler for the new list button
$("#newButton").click(function() {
	//declare a variable to define whether the user wants to save
	var shouldNew = true;

	//make sure the user wants to wipe their work
	if(!session.saved) {
		if(!showVerifyDialog('You have not saved your work. ' +
					'Continue?')) {
			shouldNew = false;
		}
	}

	//create a new plist
	if(shouldNew) {
		session.dict = new PDict(null);
		session.saved = false;
		session.path = null;
		refresh(session);
	}
});

//add an event handler for the edit radio button
$('#editMode').change(function() {
	if($(this).is(':checked')) {
		session.isEditing = true;
	}
});

//add an event handler for the delete radio button
$('#deleteMode').change(function() {
	if($(this).is(':checked')) {
		session.isEditing = false;
	}
});

//add an event handler for the collapsible list
var rootID = '#' + constants.rootDivID;
$(rootID).on('click', 'li.' + constants.collapseClass, function() {
	//toggle the siblings of the list item
	$(this).siblings('li').slideToggle('fast', function() {
		//get whether the element was hidden or shown
		var wasHidden = $(this).is(':hidden');

		//and update the toggle button to reflect the new state
		if(wasHidden) {
			$(this).siblings('li.' 
				+ constants.collapseClass).text(
						'Expand');
		} else {
			$(this).siblings('li.'
				+ constants.collapseClass).text(
						'Collapse');
		}
	});

	//and toggle the active class on the collapse button
	$(this).toggleClass('active');
});

//add an event handler for editing list elements
$(rootID).on('click', 'li.' + constants.editClass, function() {
	//get the ID of the clicked element
	var rawID = $(this).attr('id');

	//split the raw ID into its parts
	var splitID = rawID.split(':');

	//get the parent element of the ID
	var parent = objForID(splitID[0], session.dict);

	//make sure that the parent element was found successfully
	if(!parent) {
		return;
	}

	//get the key of the ID
	var key = splitID[1];

	//get the associated value and set the new value
	if(parent instanceof PArray) {
		//handle the deletion case
		if(!session.isEditing) {
			var shouldDelete = showVerifyDialog(
					'Are you sure you want to delete '
					+ parent.getKey() + ':' + key + 
					'?');
			if(shouldDelete) {
				parent.removeValueAtIndex(
					Number.parseInt(key));
				assignMember(session.dict, 
						parent.parent.idStr(),
						parent.getKey(),
						parent);
				session.saved = false;
				refresh(session);
				return;		
			}
		}

		var keyNum = Number.parseInt(key);
		var val = parent.valueAtIndex(keyNum);
		promptForValue(val).then((r) => {
			if(r) {
				if(r === "true" && 
					process.platform === "darwin") {
					assignMember(session.dict,
							splitID[0],
							keyNum,
							true);
				} else if(r === "false" &&
					process.platform === "darwin") {
					assignMember(session.dict,
							splitID[0],
							keyNum,
							false);
				} else if(!isNaN(Number.parseFloat(r))) {
					assignMember(session.dict,
							splitID[0],
							keyNum,
						Number.parseFloat(r));
				} else {
					assignMember(session.dict,
							splitID[0],
							keyNum,
							r);
				}
				refresh(session);
			}
		}).catch(console.error);
		
	} else if(parent instanceof PDict) {
		//handle the deletion case
		if(!session.isEditing) {
			var shouldDelete = showVerifyDialog(
					'Are you sure you want to delete '
					+ parent.getKey() + ':' + key + 
					'?');
			if(shouldDelete) {
				parent.removeValueForKey(key);
				assignMember(session.dict, 
						parent.parent.idStr(),
						parent.getKey(),
						parent);
				session.saved = false;
				refresh(session);
				return;		
			}
		}

		var val = parent.valueForKey(key);
		promptForValue(val).then((r) => {
			if(r) {
				if(r === "true" && 
					process.platform === "darwin") {
					assignMember(session.dict,
							splitID[0],
							key,
							true);
				} else if(r === "false" &&
					process.platform === "darwin") {
					assignMember(session.dict,
							splitID[0],
							key,
							false);
				} else if(!isNaN(Number.parseFloat(r))) {
					assignMember(session.dict,
							splitID[0],
							key,
						Number.parseFloat(r));
				} else {
					assignMember(session.dict,
							splitID[0],
							key,
							r);
				}
				refresh(session);
			}
		}).catch(console.error);
	}

	//clear the saved flag
	session.saved = false;

	//and refresh the display
	refresh(session);
});

//add an event handler for list deletion
$(rootID).on('click', 'li.' + constants.delClass, function() {
	//get the ID of the delete button
	var rawID = $(this).attr('id');

	//split it
	var splitID = rawID.split(':');

	//get the parent element of the delete class
	var parent = objForID(splitID[0], session.dict);

	//make sure that the chosen element is not the root element
	if(!parent.parent) {
		alert('The root of a property list cannot be deleted.');
		return;
	}

	//ask whether to delete the list
	var shouldDelete = showVerifyDialog('Are you sure you want to ' +
					'delete ' + parent.getKey() + '?' +
					' This action cannot be undone.');

	//handle the result
	if(shouldDelete) {
		if(parent.parent instanceof PDict) {
			//handle removing elements one layer deep
			if(!parent.parent.parent) {
				parent.parent.removeValueForKey(
						parent.getKey());
			} else {
				var tmp = parent;
				tmp.parent.removeValueForKey(
						parent.getKey());
				assignMember(session.dict,
						tmp.parent.parent.idStr(),
						tmp.parent.getKey(), 
						tmp.parent);
			}

			//update the session data
			session.saved = false;
			refresh(session);
		}

		if(parent.parent instanceof PArray) {
			//handle removing elements one layer deep
			if(!parent.parent.parent) {
				parent.parent.removeValueForKey(
						parent.getKey());
			} else {
				var tmp = parent;
				tmp.parent.removeValueAtIndex(
					Number.parseInt(parent.getKey()));
				assignMember(session.dict,
						tmp.parent.parent.idStr(),
						tmp.parent.getKey(), 
						tmp.parent);
			}

			//update the session data
			session.saved = false;
			refresh(session);
		}

	}
});

//add an event handler for the Add Element button
$(rootID).on('click', 'li.' + constants.addClass, function() {
	//get the raw ID of the add button
	var rawID = $(this).attr('id');

	//split the ID
	var splitID = rawID.split(':');

	//get the parent object of the add element button
	var parent = objForID(splitID[0], session.dict);

	//get the type of element to add
	var elemType = showAddDialog();

	//make sure a type was chosen
	if(elemType === AddDialogValues.CANCEL) {
		return;
	}

	//handle different parent types
	if(parent instanceof PDict) {
		//handle the parent being the root element
		var isRoot = false;
		if(!parent.parent) {
			isRoot = true;
		}

		//prompt for the key and value to add
		promptForKey().then((k) => {
			//if no key is supplied, or if a dictionary
			//or array should be added,
			//then don't prompt for a value
			if(k && ((elemType !== AddDialogValues.ARRAY) &&
				(elemType !== AddDialogValues.DICT))){
				promptForValue().then((v) => {
					//get the value to add
					var val = v;
					if(elemType === 
						AddDialogValues.NUMBER) {
						val = Number.parseFloat(v);
					} else if(elemType ===
						AddDialogValues.BOOLEAN) {
						if(v === "true") {
							val = true;
						} else {
							val = false;
						}
					}

					if(isRoot) {
						parent.addValue(k, val);
					} else {
						parent.addValue(k, val);
						assignMember(session.dict,
						parent.parent.idStr(),
							parent.getKey(),
							parent);
					}

					//refresh the session
					session.saved = false;
					refresh(session);
				}).catch(console.error);
			}

			//handle adding an array or dictionary
			if(elemType === AddDialogValues.ARRAY) {
				parent.addValue(k, new PArray(parent));

				if(!isRoot) {
					assignMember(session.dict,
						parent.parent.idStr(),
						parent.getKey(),
						parent);
				}
				session.saved = false;
				refresh(session);
			} else if(elemType === AddDialogValues.DICT) {
				parent.addValue(k, new PDict(parent));
				if(!isRoot) {
					assignMember(session.dict,
						parent.parent.idStr(),
						parent.getKey(),
						parent);
				}
				session.saved = false;
				refresh(session);
			}

		}).catch(console.error);
	} else if(parent instanceof PArray) {
		//handle the parent being the root element
		var isRoot = false;
		if(!parent.parent) {
			isRoot = true;
		}

		//prompt for the value to add, or add a dict or array
		if(elemType === AddDialogValues.DICT) {
			parent.addValue(parent.nextIndex(),
					new PDict(parent));
			if(!isRoot) {
				assignMember(session.dict,
						parent.parent.idStr(),
						parent.getKey(),
						parent);
			}

			session.saved = false;
			refresh(session);
		} else if(elemType === AddDialogValues.ARRAY) {
			parent.addValue(parent.nextIndex(),
					new PArray(parent));
			if(!isRoot) {
				assignMember(session.dict,
						parent.parent.idStr(),
						parent.getKey(),
						parent);
			}

			session.saved = false;
			refresh(session);
		} else { //number, boolean, or string selected
			promptForValue().then((v) => {
				//convert the value to a number
				//if need be
				var val = v;
				if(elemType === AddDialogValues.NUMBER) {
					val = Number.parseFloat(v);
				} else if(elemType === 
						AddDialogValues.BOOLEAN) {
					if(v === "true") {
						val = true;
					} else {
						val = false;
					}
				}

				//add the value to the parent
				parent.addValue(parent.nextIndex(), val);

				//assign in the parent if need be
				if(!isRoot) {
					assignMember(session.dict,
						parent.parent.idStr(),
						parent.getKey(),
						parent);
				}

				//and refresh the session
				session.saved = false;
				refresh(session);
			}).catch(console.error);
		}
	}
});

//end of file
