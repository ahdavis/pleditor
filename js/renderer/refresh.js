/*
 * refresh.js
 * Defines a function that refreshes the plist view
 * Created on 2/24/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
var constants = require('../util/constants.js');
var UserSession = require('../user/UserSession.js').UserSession;

//this function refreshes the display
function refresh(session) {
	if(session instanceof UserSession) {
		//set the html of the root div to the HTML
		//emitted by the session dictionary
		const rootID = '#' + constants.rootDivID;
		$(rootID).html(session.dict.toHTML());
		
		//get the filename of the currently edited file
		var filename = 'New File';
		if(session.path) {	
			filename = session.path.replace(/^.*[\\\/]/, '');
		}

		//and set the filename paragraph to the filename
		$("#fileName").text('Currently editing: ' + filename);
	}
}

//export
exports.refresh = refresh;

//end of file
