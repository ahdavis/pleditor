/*
 * PDict.js
 * Defines a class that represents a property dictionary
 * Created on 2/18/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//import
var fs = require('fs');
var xmlparser = require('fast-xml-parser');
var GNULexer = require('../lang/GNULexer.js').GNULexer;
var GNUParser = require('../lang/GNUParser.js').GNUParser;
var constants = require('../util/constants.js');

//class definition
class PDict {
	//class constructor
	constructor(parent) {
		//init the fields
		this.data = new Map();
		this.id = PDict.curID++;
		this.parent = parent;
	}

	//Returns the ID string for the dictionary
	idStr() {
		return 'PDict' + this.id.toString();
	}

	//returns the key for the dictionary
	getKey() {
		var ret = null;
		if(!this.parent) {
			ret = 'root';
		} else {
			for(const [k, v] of this.parent) {
				if(v instanceof PDict) {
					if(v.idStr() === this.idStr()) {
						ret = k;
					}
				}
			}
		}

		return ret;
	}

	//Adds a value to the dictionary.
	//The value must be a number, a string, a boolean,
	//a PArray, or a PDict. The key must be a string.
	//Returns whether the value was added successfully.
	addValue(key, value) {
		//validate the type
		if((typeof value !== "string") &&
			(typeof value !== "number") &&
			(!(value instanceof PArray)) &&
			(!(value instanceof PDict))) {
			return false;
		}

		//validate the type of the key
		if(typeof key !== "string") {
			return false;
		}

		//add the value to the array
		this.data.set(key, value);

		//and return a success
		return true;

	}

	//returns the value associated with a given key
	//returns null if the key is not in the dictionary
	valueForKey(key) {
		return this.data.get(key);
	}

	//removes the value associated with a key
	removeValueForKey(key) {
		this.data.delete(key);
	}

	//this method loads a dictionary from a file on any platform
	loadFromFile(path) {
		//handle different platforms
		if(process.platform === "darwin") {
			this.loadFromFileOnMac(path);
		} else {
			this.loadFromFileOnGNUStep(path);
		}
	}

	//this method loads a dictionary from a file on Linux or Windows
	loadFromFileOnGNUStep(path) {
		//read in the file
		var text = fs.readFileSync(path, 'utf-8');

		//lex it
		var lexer = new GNULexer(text);
		
		//parse it
		var parser = new GNUParser(lexer);
		var ast = parser.parse();

		//and load the dictionary from the evaluated AST
		this.loadFromJson(ast.evaluate());
	}

	//This method loads a dictionary from a file on macOS
	loadFromFileOnMac(path) {
		//read in the file
		var text = fs.readFileSync(path, 'utf-8');

		//convert the text to json amd load it into the dictionary
		var xmlData = xmlparser.getTraversalObj(text);
		this.loadFromJson(xmlparser.convertToJsonString(xmlData));
	}


	//this function loads the dictionary from json
	loadFromJson(json) {
		//get the JS object form of the json
		var jsobj = JSON.parse(json);

		//and process it
		this.loadFromObject(jsobj);
	}

	//this function acts as a helper function
	//to load the dictionary from json recursively
	loadFromObject(obj) {
		//loop through the object
		for(var key in obj) {
			if(obj.hasOwnProperty(key)) {
				//get the value
				var val = obj[key];

				//handle its different types
				if(typeof val === "number") {
					this.addValue(key, val);
				} else if(typeof val === "string") {
					this.addValue(key, val);
				} else if(typeof val === "boolean") {
					this.addValue(key, val);
				} else if(val instanceof Array) {
					var addArr = new PArray(this);
					addArr.loadFromObject(val);
					this.addValue(key, addArr);
				} else if(val instanceof Object) {
					var addDict = new PDict(this);
					addDict.loadFromObject(val);
					this.addValue(key, addDict);
				}
			}
		}
	}

	//writes the dictionary to a given file
	writeToFile(path) {
		var fd = fs.openSync(path, 'w'); //open the file
		this.writeToFD(fd); //write the dictionary to the file
		fs.closeSync(fd); //and close the file
	}

	//does the actual work of writing the dictionary
	//to a file descriptor
	writeToFD(fd) {
		//get whether the app is being run on macOS
		var isMac = (process.platform === 'darwin');

		//write the start of the plist on macOS
		//if this object is the root of the plist
		if(isMac && !this.parent) {
			fs.writeSync(fd, 
				'<?xml version="1.0" encoding="UTF-8"?>');
			fs.writeSync(fd, '<!DOCTYPE plist PUBLIC ' + 
				'"-//Apple//DTD PLIST 1.0//EN" ' + 
		'"http://www.apple.com/DTDs/PropertyList-1.0.dtd">');
			fs.writeSync(fd, '<plist version="1.0">');
		}

		//write the dictionary start
		if(isMac) {
			fs.writeSync(fd, '<dict>');
		} else {
			fs.writeSync(fd, '{ ');
		}

		//loop and write the dictionary data
		for(let [key, val] of this.data) {
			//write the key to the file
			if(isMac) {
				fs.writeSync(fd, '<key>' + key + '</key>');
			} else {
				fs.writeSync(fd, key + ' = ');
			}

			//handle its types
			if(typeof val === "string") {
				if(isMac) {
					fs.writeSync(fd,
						'<string>' +
						val + '</string>');
				} else {
					fs.writeSync(fd,
						'"' + val + '"; ');
				}
			} else if(typeof val === "number") {
				if(isMac) {
					fs.writeSync(fd,
						'<real>' +
						val.toString() +
						'</real>');
				} else {
					fs.writeSync(fd, 
						val.toString() +
						'; ');
				}
			} else if(isMac && typeof val === "boolean") {
				var valStr = val ? '<true/>' : '<false/>';
				fs.writeSync(fd, val);
			} else if(val instanceof PArray) {
				val.writeToFD(fd);
				if(!isMac) {
					fs.writeSync(fd, ';');
				}
			} else if(val instanceof PDict) {
				val.writeToFD(fd);
				if(!isMac) {
					fs.writeSync(fd, ';');
				}
			}
		}

		//write the end of the dictionary
		if(isMac) {
			fs.writeSync(fd, '</dict>');
			if(!this.parent) {
				fs.writeSync(fd, '</plist>');
			}
		} else {
			fs.writeSync(fd, ' }');
		}	
	}

	//returns an HTML representation of the dictionary
	toHTML() {
		//declare the return value
		var ret = '<ul id="' + this.idStr() + '">';

		//add a delete button if this is not the root node
		if(this.parent) {
			ret += '<li class="' + constants.delClass 
			+ '" id="' +
			this.idStr() + ':del' + '">' +
			'Delete...' + '</li>';
		}

		//add a collapse button
		ret += '<li class="' + constants.collapseClass + '">' +
			'Collapse' +  '</li>';
	

		//generate the HTML for the dictionary
		for(let [key, obj] of this.data) {
			//handle its types
			if(typeof obj === "string") {
				//get the class for the string
				var strClass = constants.editClass;

				//emit the list element
				ret += '<li class="' + strClass +
					'" id="' + this.idStr() 
					+ ':' 
					+ key + '">'
					+ key + ': ' +
						obj + '</li>';
			} else if(typeof obj === "number") {
				//get the class for the number
				var numClass = constants.editClass;

				//emit the list element
				ret += '<li class="' + numClass +
					'" id="' + this.idStr() 
					+ ':'
					+ key +'">'
					+ key + ': ' + 
					obj.toString() +
					'</li>';
			} else if(typeof obj === "boolean") {
				var objStr = obj ? 'true' : 'false';

				ret += '<li class="' +
					constants.editClass + 
					this.idStr() + ':'
					+ key + '">' + key + ': ' +
					objStr + '</li>';
			} else if(obj instanceof PArray) {
				ret += '<li id="' + this.idStr() +
					':' + key +
					'">' +
					key + ': ' +
					obj.toHTML() + '</li>';
			} else if(obj instanceof PDict) {
				ret += '<li id="' + this.idStr() +
					':' + key +
					'">' +
					key + ': ' +
					obj.toHTML() + '</li>';
			}
		}
		
		//append an add element button
		ret += '<li class="' + constants.addClass + '" id="' +
			this.idStr() + ':add">';
		ret += 'Add Element...';
		ret += '</li>';

		//append the end of the list
		ret += '</ul>';

		//and return the list
		return ret;
	}

	//returns an iterator for the dictionary
	[Symbol.iterator]() {
		//return an iterator for the data member
		return this.data.entries();
	}
};

//add the static field
PDict.curID = 0;

//export the class
exports.PDict = PDict;

//import the PArray class
var PArray = require('./PArray.js').PArray;

//end of class
