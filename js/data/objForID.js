/*
 * objForID.js
 * Defines a function that returns the plist object associated with an ID
 * Created on 2/26/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
const PDict = require('./PDict.js').PDict;
const PArray = require('./PArray.js').PArray;

//returns the PDict or PArray object corresponding to an ID string
//returns null if no such object is found
function objForID(id, node) {
	//make sure that the root argument is a PDict or PArray
	if((!(node instanceof PDict)) && (!(node instanceof PArray))) {
		return null;
	}

	//declare the return value
	var ret = null;

	//declare a variable to hold whether the object was found
	var found = false;

	//check the node for its ID
	if(node.idStr() === id) {
		ret = node;
		found = true;
	}

	//process the node
	for(const [k, v] of node) {
		//handle different node type
		if(v instanceof PArray) {
			//only search if the object was not found
			if(!found) {
				ret = objForID(id, v);
			}

			//if ret is not null, then the object was found
			if(ret) {
				found = true;
			}
		} else if(v instanceof PDict) {
			//only search if the object was not found
			if(!found) {
				ret = objForID(id, v);
			}

			//if ret is not null, then the object was found
			if(ret) {
				found = true;
			}
		}
	}

	//return the object
	return ret;
}

//export
exports.objForID = objForID;

//end of file
