/*
 * pathToElement.js
 * Defines a function that returns the path to a plist container element
 * Created on 2/26/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
const PArray = require('./PArray.js').PArray;
const PDict = require('./PDict.js').PDict;
const objForID = require('./objForID.js').objForID;

//returns the dot-seperated path to a plist element
//returns null if no list element corresponds to the id
function pathToElement(id, root) {
	//get the element with the given id
	var elem = objForID(id, root);

	//handle a null element
	if(!elem) {
		return null;
	}

	//and return the path
	return pathToElemHelper(elem);
}

//recursively obtains the path to a plist element
function pathToElemHelper(elem) {
	//make sure that the element is a PArray or PDict
	if((!(elem instanceof PArray)) && (!(elem instanceof PDict))) {
		return null;
	}

	//handle the element being the root element
	if(!elem.parent) {
		return 'root';
	}

	//get the element's key
	var key = elem.getKey();

	//recursively obtain the key string
	return pathToElemHelper(elem.parent) + '.' + key;
}


//export
exports.pathToElement = pathToElement;

//end of file
