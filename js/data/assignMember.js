/*
 * assignMember.js
 * Defines a function that assigns a value to a property list data member
 * Created on 2/26/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
const PArray = require('./PArray.js').PArray;
const PDict = require('./PDict.js').PDict;
const pathToElement = require('./pathToElement.js').pathToElement;

//Assigns a value to a key found in any depth of
//the argument structure. Returns whether the assignment was successful
function assignMember(root, id, key, member) {
	//get the path to the modification struct
	var path = pathToElement(id, root); 

	//and call the helper function
	return assignMemberHelper(root, path.split('.'), 1, key, member);
}

//Helper function for the assignMember function
function assignMemberHelper(struct, pathArr, depth, key, member) {
	//declare the return value
	var ret = false;

	//handle different structure types
	if(struct instanceof PArray) {
		//loop through the array
		for(let [i, val] of struct) {
			//get the current element of the path array
			var curPath = pathArr[depth];

			//handle different types of values
			if((typeof val === "string") &&
				(i === key)) {
				struct.addValue(i, member);
				ret = true;
			} else if((typeof val === "number") &&
				(i === key)) {
				struct.addValue(i, member);
				ret = true;
			} else if(val instanceof PArray) {
				if(i === key) {
					struct.addValue(i, member);
					ret = true;
				} else if(val.getKey() === curPath) {
					ret = assignMemberHelper(val, 
							pathArr.slice(1),
							depth + 1,
							key,
							member);
				} else {
					ret = false;
				}
			} else if(val instanceof PDict) {
				if(i === key) {
					struct.addValue(i, member);
					ret = true;
				} else if(val.getKey() === curPath) {
					ret = assignMemberHelper(val,
							pathArr.slice(1),
							depth + 1,
							key,
							member);
				} else {
					ret = false;
				}
			}
		}

		//no assignment, so return false
		ret = false;
	} else if(struct instanceof PDict) {
		//loop through the dictionary
		for(let [k, v] of struct) {
			//get the current path key
			curPath = pathArr[depth];

			//handle different value types
			if((typeof v === "string") && (k === key)) {
				struct.addValue(k, member);
				ret = true;
			} else if((typeof v === "number") &&
					(k === key)) {
				struct.addValue(k, member);
				ret = true;
			} else if(v instanceof PArray) {
				if(k === key) {
					struct.addValue(k, member);
					ret = true;
				} else if(v.getKey() === curPath) {
					ret = assignMemberHelper(v,
							pathArr.slice(1),
							depth + 1,
							key,
							member);
				} else {
					ret = false;
				}
			} else if(v instanceof PDict) {
				if(k === key) {
					struct.addValue(k, member);
					ret = true;
				} else if(v.getKey() === curPath) {
					ret = assignMemberHelper(v,
							pathArr.slice(1),
							depth + 1,
							key,
							member);
				} else {
					ret = false;
				}
			}
		}

		//no assignment, so return false
		ret = false;
	} else { //invalid object
		ret = false;
	}

	//return the result
	return ret;
}

//export
exports.assignMember = assignMember;

//end of file
