/*
 * PArray.js
 * Defines a class that represents a property array
 * Created on 2/18/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
var fs = require('fs');
const constants = require('../util/constants.js');

//class definition
class PArray {
	//class constructor
	constructor(parent) {
		//init the fields
		this.data = new Array();
		this.id = PArray.curID++;
		this.parent = parent;
	}

	//returns the ID string for the array
	idStr() {
		return 'PArray' + this.id.toString();
	}

	//returns the key for the array
	getKey() {
		var ret = null;
		if(!this.parent) {
			ret = 'root';
		} else {
			for(const [k, v] of this.parent) {
				if(v instanceof PArray) {
					if(v.idStr() === this.idStr()) {
						ret = k;
					}
				}
			}
		}
		
		return ret;
	}

	//this function returns the index of the next slot 
	//in the array to add items to
	nextIndex() {
		return this.data.length;
	}

	//this function acts as a helper function
	//to load the array from json recursively
	loadFromObject(obj) {
		//declare variables
		var i = 0; //index variable for loading data
		
		//loop through the object
		for(var key in obj) {
			if(obj.hasOwnProperty(key)) {
				//get the value
				var val = obj[key];

				//handle its different types
				if(typeof val === "number") {
					this.addValue(i++, val);
				} else if(typeof val === "string") {
					this.addValue(i++, val);
				} else if(typeof val === "boolean") {
					this.addValue(i++, val);
				} else if(val instanceof Array) {
					var addArr = new PArray(this);
					addArr.loadFromObject(val);
					this.addValue(i++, addArr);
				} else if(val instanceof Object) {
					var addDict = new PDict(this);
					addDict.loadFromObject(val);
					this.addValue(i++, addDict);
				}
			}
		}
	}

	//This method adds a value to the array.
	//The value must be a number, a string, a boolean,
	//a PArray, or a PDict. Returns whether the value
	//was added successfully
	addValue(idx, val) {
		//validate the type
		if((typeof val !== "string") &&
			(typeof val !== "number") &&
			(!(val instanceof PArray)) &&
			(!(val instanceof PDict))) {
			return false;
		}

		//add the value to the array
		this.data[idx] = val;

		//and return a success
		return true;
	}

	//returns the value at a given index into the array
	//returns null if the index is out of bounds
	valueAtIndex(idx) {
		//handle out of bounds index cases
		if((idx < 0) || (idx >= this.data.length)) {
			//index is out of bounds, so return null
			return null;
		} else {
			//index is in bounds, so return the value
			//referenced by it
			return this.data[idx];
		}
	}

	//removes the value at an index
	removeValueAtIndex(idx) {
		this.data.splice(idx, 1);
	}

	//writes the array to a file
	writeToFile(path) {
		var fd = fs.openSync(path, 'w'); //open the file
		this.writeToFD(fd); //write it to a file
		fs.closeSync(fd); //and close it
	}

	//does the actual work of writing the array to a file
	writeToFD(fd) {
		//get whether the app is running on macOS
		var isMac = (process.platform === 'darwin');

		//write the start of the array 	
		if(isMac) {
			fs.writeSync(fd, '<array>');
		} else {
			fs.writeSync(fd, '(');
		}

		//loop and write the array to a file
		for(var i = 0; i < this.data.length; i++) {
			//get the current value
			var val = this.data[i];

			//handle its types
			if(typeof val === "string") {
				if(isMac) {
					fs.writeSync(fd, '<string>' +
							val + '</string>');
				} else {
					fs.writeSync(fd, '"' + val + '"');
				}
			} else if(typeof val === "number") {
				if(isMac) {
					fs.writeSync(fd, '<real>'+
							val.toString() +
							'</real>');
				} else {
					fs.writeSync(fd, val.toString());
				}
			} else if(isMac && typeof val === "boolean") {
				if(val === true) {
					fs.writeSync(fd, '<true/>');
				} else {
					fs.writeSync(fd, '<false/>');
				}
			} else if(val instanceof PDict) {
				val.writeToFD(fd);
			} else if(val instanceof PArray) {
				val.writeToFD(fd);
			}

			//write a comma after the item if the 
			//index is not at the end of the array
			//and only if the app is not being run on macOS
			if((i < (this.data.length - 1)) && !isMac) {
				fs.writeSync(fd, ', ');
			}
		}

		//write the end of the array
		if(isMac) {
			fs.writeSync(fd, '</array>');
		} else {
			fs.writeSync(fd, ')');
		}
	}

	//returns a string containing the HTML representation
	//of the array
	toHTML() {
		//declare the return value
		var ret = '<ul id="' + this.idStr() + '">';

		//add a delete button
		ret += '<li class="' + constants.delClass + '" id="' +
			this.idStr() + ':del' + '">'
			+ 'Delete...' + '</li>';

		//add a collapse button
		ret += '<li class="' + constants.collapseClass + '">'
			+ 'Collapse' + '</li>';

		//emit HTML code for the array
		for(var i = 0; i < this.data.length; i++) {
			//get the current object
			var obj = this.data[i];

			//handle its types
			if(typeof obj === "string") {
				ret += '<li class="' + 
					constants.editClass +
					'" id="' + this.idStr() + ':' +
					i.toString() +
					'">' + obj + '</li>';
			} else if(typeof obj === "number") {
				ret += '<li class="' +
					constants.editClass +
					'" id="' + this.idStr() + ':' +
					i.toString() +
					'">' + obj.toString() + '</li>';
			} else if(typeof obj === "boolean") {
				var objStr = obj ? 'true' : 'false';
				ret += '<li class="' +
					constants.editClass +
					'" id="' + this.idStr() + ':' +
					i.toString() +
					'">' + objStr + '</li>';
			} else if(obj instanceof PArray) {
				var cls = constants.editClass;
				ret += '<li id="' + this.idStr() + ':' +
					i.toString() +
					'">' + obj.toHTML() + '</li>';
			} else if(obj instanceof PDict) {
				var cls = constants.editClass;
				ret += '<li id="' + this.idStr() + ':' +
					i.toString() +
					'">' + obj.toHTML() + '</li>'; 
			}
		}

		//append an add element button
		ret += '<li class="' + constants.addClass + '" id="' +
			this.idStr() + ':add">';
		ret += 'Add Element...';
		ret += '</li>';

		//append the end of the list
		ret += '</ul>';

		//and return the list
		return ret;	
	}

	//returns an iterator for the array
	[Symbol.iterator]() {
		//return an iterator for the inner data array
		return this.data.entries();	
	}
};

//define the static property
PArray.curID = 0;

//export the class
exports.PArray = PArray;

//import the PDict class
var PDict = require('./PDict.js').PDict;

//end of file
