/*
 * NodeType.js
 * Defines types of AST nodes
 * Created on 2/20/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//no imports

//constant definitions
const N_NUM = 0; //number value node
const N_STR = 1; //string value node
const N_KEY = 2; //dictionary key node
const N_DICTENT = 3; //dictionary entry node
const N_DICT = 4; //dictionary node
const N_KVPAIR = 5; //dictionary key/value pair node
const N_ARRENT = 6; //array entry node
const N_ARR = 7; //array node

//exports
exports.N_NUM = N_NUM;
exports.N_STR = N_STR;
exports.N_KEY = N_KEY;
exports.N_DICTENT = N_DICTENT;
exports.N_DICT = N_DICT;
exports.N_KVPAIR = N_KVPAIR;
exports.N_ARRENT = N_ARRENT;
exports.N_ARR = N_ARR;

//end of file
