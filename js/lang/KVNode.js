/*
 * KVNode.js
 * Defines a class that represents an AST node for a key/value pair
 * Created on 2/20/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
const NodeType = require('./NodeType.js');
const ASTNode = require('./ASTNode.js').ASTNode;

//class definition
class KVNode extends ASTNode {
	//constructor
	constructor(key, value) {
		//call the superclass constructor
		super(NodeType.N_KVPAIR, null, value);

		//and init the field
		this.key = new String(key.evaluate());
	}

	//evaluates the node
	evaluate() {
		//get the key name wrapped in quotes
		var keyName = '"' + this.key + '"';

		//assemble the JSON code for the pair
		var ret = keyName + ':' + this.right.evaluate();

		//and return it
		return ret;
	}
};

//export
exports.KVNode = KVNode;

//end of file
