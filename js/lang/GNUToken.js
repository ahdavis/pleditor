/*
 * GNUToken.js
 * Defines a class that represents a GNUStep plist parser token
 * Created on 2/20/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//import
const TokenType = require('./TokenType.js');

//class definition
class GNUToken {
	//constructor
	constructor(type, value) {
		//init the fields
		this.type = type;
		this.value = value;
	}

	//getter methods
	
	//returns the type of the token
	getType() {
		return this.type;
	}

	//returns the value of the token
	getValue() {
		return this.value;
	}
};

//export
exports.GNUToken = GNUToken;

//end of file
