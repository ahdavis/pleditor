/*
 * GNUParser.js
 * Defines a class that parses GNUStep property lists
 * Created on 2/20/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
const TokenType = require('./TokenType.js');
const GNUToken = require('./GNUToken.js').GNUToken;
const GNULexer = require('./GNULexer.js').GNULexer;
const NumNode = require('./NumNode.js').NumNode;
const StrNode = require('./StrNode.js').StrNode;
const KeyNode = require('./KeyNode.js').KeyNode;
const KVNode = require('./KVNode.js').KVNode;
const DictEntNode = require('./DictEntNode.js').DictEntNode;
const DictNode = require('./DictNode.js').DictNode;
const ArrayEntNode = require('./ArrayEntNode.js').ArrayEntNode;
const ArrayNode = require('./ArrayNode.js').ArrayNode;
const isdigit = require('../util/isdigit.js').isdigit;

//class definition
class GNUParser {
	//constructor
	constructor(lexer) {
		//init the fields
		this.lexer = lexer; //the lexer to obtain tokens from
		this.curToken = this.lexer.getNextToken(); 
	}

	//parses plist code and returns an AST
	//representing the code
	parse() {
		return this.plist();
	}

	//All methods beyond this point should not be called
	//from outside the Parser class
	
	//verifies the current token and gets the next token
	//throws an error on a syntax error
	eat(tokenType) {
		//compare the current type with the argument type
		if(this.curToken.getType() === tokenType) {
			//get the next token
			this.curToken = this.lexer.getNextToken();
		} else {
			throw new Error('Syntax error');
		}
	}

	//parses a full plist
	plist() {
		//declare the result
		var ret = null;

		//parse the opening symbol
		if(this.curToken.getType() === TokenType.T_LBRACE) { //dict
			//process the open brace
			this.eat(TokenType.T_LBRACE);

			//process the dictionary
			if(this.curToken.getType() 
				!== TokenType.T_RBRACE) {
				ret = new DictNode(this.dict());
			} else {
				ret = new DictNode(null);
			}

			//and process the close brace
			this.eat(TokenType.T_RBRACE);
		} else if(this.curToken.getType() === TokenType.T_LPAREN) {
			//array found
			
			//process the open parenthesis
			this.eat(TokenType.T_LPAREN);

			//process the array
			if(this.curToken.getType()
				!== TokenType.T_RPAREN) {
				ret = new ArrayNode(this.array());
			} else {
				ret = new ArrayNode(null);
			}

			//and process the close parenthesis
			this.eat(TokenType.T_RPAREN);
		} else { //invalid format
			throw new Error('Invalid plist format');
		}

		//return the parsed plist AST
		return ret;
	}

	//parses an array
	array() {
		//get the AST of the current object in the array
		var valNode = this.value();

		//declare the return AST
		var node = null;

		//see if the end of the array was reached
		if(this.curToken.getType() === TokenType.T_COMMA) {
			//end not reached
			
			//consume the comma
			this.eat(TokenType.T_COMMA);

			//and recursively create the array AST
			node = new ArrayEntNode(valNode, this.array());
		} else {
			//end reached
			
			//return the final node in the array
			node = new ArrayEntNode(valNode, null);
		}

		//and return the assembled array AST
		return node;
	}

	//parses a dictionary
	dict() {
		//get the first pair of the dictionary
		var pairNode = this.kvpair();

		//declare the return AST
		var node = null;

		//see if the end of the dictionary was reached
		if(this.curToken.getType() !== TokenType.T_RBRACE) {
			//end not reached
			
			//generate the dictionary recursively
			node = new DictEntNode(pairNode, this.dict());
		} else {
			//end reached
			
			//generate the end of the dictionary
			node = new DictEntNode(pairNode, null);
		}

		//and return the assembled array AST
		return node;
	}

	//parses a key/value pair
	kvpair() {
		//get the key for the pair
		var keyNode = this.key();

		//parse the equals sign
		this.eat(TokenType.T_EQU);

		//get the value for the pair
		var valNode = this.value();

		//parse the semicolon
		this.eat(TokenType.T_SEMIC);

		//and return a KVNode object
		return new KVNode(keyNode, valNode);
	}

	//parses a key
	key() {
		//save the current token
		var token = this.curToken;

		//parse a key
		this.eat(TokenType.T_KEY);

		//and return a KeyNode object from the token
		return new KeyNode(token.getValue());
	}

	//parses a value
	value() {
		//save the current token
		var token = this.curToken;

		//declare the return value
		var ret = null;

		//check for different value types
		if(token.getType() === TokenType.T_STR) { //string found
			this.eat(TokenType.T_STR); //process the string

			//and return a string node
			ret = new StrNode(token.getValue());
		} else if(token.getType() === TokenType.T_NUM) { //number
			this.eat(TokenType.T_NUM); //process the number

			//and return a number node
			ret = new NumNode(token.getValue());
		} else if((token.getType() === TokenType.T_LPAREN) ||
				(token.getType() === TokenType.T_LBRACE)) {
			//nested property list found
			ret = this.plist();
		}

		//and return the value
		return ret;
	}
};

//export
exports.GNUParser = GNUParser;

//end of file
