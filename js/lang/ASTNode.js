/*
 * ASTNode.js
 * Defines a class that represents an AST node
 * Created on 2/20/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//no imports

//class definition
class ASTNode {
	//constructor
	constructor(type, left, right) {
		this.type = type; //the type of the node
		this.left = left; //the left branch of the AST
		this.right = right; //the right branch of the AST
	}

	//getter method
	
	//returns the type of the node
	getType() {
		return this.type;
	}

	//evaluates the node and returns a JSON object (as a string)
	//based on the tree
	//must be overridden in subclasses
	evaluate() {
		throw new Error("ASTNode::evaluate() must be overridden!");
	}
};

//export
exports.ASTNode = ASTNode;

//end of file
