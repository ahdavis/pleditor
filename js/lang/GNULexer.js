/*
 * GNULexer.js
 * Defines a class that lexes GNUStep property lists
 * Created on 2/20/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
const TokenType = require('./TokenType.js');
const GNUToken = require('./GNUToken.js').GNUToken;
const isspace = require('../util/isspace.js').isspace;
const isalpha = require('../util/isalpha.js').isalpha;
const isdigit = require('../util/isdigit.js').isdigit;

//class definition
class GNULexer {
	//constructor
	constructor(text) {
		this.text = new String(text); //the text to lex
		this.pos = 0; //the current index into the text
		this.curChar = this.text.charAt(this.pos);
	}

	//Returns the next token consumed from the input text
	//Throws an Error if an invalid character is found
	getNextToken() {
		//loop through the input
		while(this.curChar !== null) {
			//handle spaces
			if(isspace(this.curChar)) {
				this.skipWhitespace();
				continue;
			}

			//handle numbers
			if(isdigit(this.curChar)) {
				return new GNUToken(TokenType.T_NUM,
							this.number());
			}

			//handle strings
			if(this.curChar === '"') {
				return new GNUToken(TokenType.T_STR,
							this.string());
			}

			//handle dictionary keys
			if(isalpha(this.curChar)) {
				return new GNUToken(TokenType.T_KEY,
							this.key());
			}

			//handle left braces
			if(this.curChar === '{') {
				this.advance();
				return new GNUToken(TokenType.T_LBRACE, 
							'{');
			}

			//handle right braces
			if(this.curChar === '}') {
				this.advance();
				return new GNUToken(TokenType.T_RBRACE, 
							'}');
			}

			//handle left parentheses
			if(this.curChar === '(') {
				this.advance();
				return new GNUToken(TokenType.T_LPAREN, 
							'(');
			}

			//handle right parentheses
			if(this.curChar === ')') {
				this.advance();
				return new GNUToken(TokenType.T_RPAREN, 
							')');
			}

			//handle commas
			if(this.curChar === ',') {
				this.advance();
				return new GNUToken(TokenType.T_COMMA, 
							',');
			}

			//handle equals signs
			if(this.curChar === '=') {
				this.advance();
				return new GNUToken(TokenType.T_EQU, 
							'=');
			}

			//handle semicolons
			if(this.curChar === ';') {
				this.advance();
				return new GNUToken(TokenType.T_SEMIC, 
							';');
			}

			//if control reaches here, then an invalid
			//character was found in the text
			throw new Error('Unknown character ' +
						this.curChar);
		}

		//handle EOT
		return new GNUToken(TokenType.T_EOT, null);
	}

	//All methods past this point should not be called
	//from outside this class definition.
	//JS has no mechanism for private methods,
	//so I have to make do.

	//Advances the lexer and gets the next character
	advance() {
		this.pos++; //increment the position counter
		
		//handle end of input
		if(this.pos > (this.text.length - 1)) {
			this.curChar = null;
		} else {
			//get the next character
			this.curChar = this.text.charAt(this.pos);
		}
	}

	//Skips any whitespace in the text
	skipWhitespace() {
		//loop through the whitespace
		while((this.curChar !== null) && isspace(this.curChar)) {
			this.advance();
		}
	}

	//Returns a key string consumed from the input
	key() {
		//declare the return value
		var ret = new String();

		//loop and assemble the return value
		while((this.curChar !== null) && (isalpha(this.curChar) ||
			isdigit(this.curChar))) {
			ret += this.curChar;
			this.advance();
		}

		//and return the assembled key
		return ret;
	}

	//Returns a data string consumed from the input
	string() {
		//declare the return value
		var ret = new String();

		//advance past the initial quotation
		this.advance();

		//assemble the string
		while((this.curChar !== null) && (this.curChar !== '"')) {
			ret += this.curChar;
			this.advance();
		}

		//advance past the ending quotation
		this.advance();

		//and return the assembled string
		return ret;
	}

	//Returns a number consumed from the input
	//Throws an exception if multiple decimal points are found
	//in the same number
	number() {
		//declare variables
		var retStr = ''; //the number as a string
		var foundPoint = false; //has a decimal point been found?

		//loop and assemble the number
		while((this.curChar !== null) && ((isdigit(this.curChar)) 
					|| (this.curChar === '.'))) {
			//check for a decimal
			if(this.curChar === '.') {
				//check for multiple decimals
				if(foundPoint) { 
					throw new Error(
					'Multiple decimal points found!');
				}

				//set the point flag
				foundPoint = true;

				//and add the decimal to the string
				retStr += this.curChar;
			} else {
				//character is a digit
				retStr += this.curChar;
			}

			//advance the lexer
			this.advance();
		}

		//and determine whether a float or an int was found
		if(foundPoint) { //float
			return parseFloat(retStr);
		} else { //int
			return parseInt(retStr);
		}
	}

};

//export the class
exports.GNULexer = GNULexer;

//end of file
