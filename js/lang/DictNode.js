/*
 * DictNode.js
 * Defines a class that represents an AST node for a dictionary
 * Created on 2/20/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
const NodeType = require('./NodeType.js');
const ASTNode = require('./ASTNode.js').ASTNode;

//class definition
class DictNode extends ASTNode {
	//constructor
	constructor(data) {
		//call the superclass constructor
		super(NodeType.N_DICT, null, data);
	}

	//evaluates the node
	evaluate() {
		//create a string with the start of the dictionary
		var ret = '{ ';

		//append the JSON text for the rest of the dictionary
		if(this.right) {
			ret += this.right.evaluate();
		}

		//append the end of the dictionary
		ret += ' }';

		//and return the assembled text
		return ret;
	}
};

//export
exports.DictNode = DictNode;

//end of file
