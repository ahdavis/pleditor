/*
 * KeyNode.js
 * Defines a class that represents a dictionary key AST node
 * Created on 2/20/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
const NodeType = require('./NodeType.js');
const ASTNode = require('./ASTNode.js').ASTNode;

//class definition
class KeyNode extends ASTNode {
	//constructor
	constructor(value) {
		//call the superclass constructor
		super(NodeType.N_KEY, null, null);

		//and init the field
		this.value = new String(value); //the value of the node
	}

	//getter method
	
	//returns the value of the node
	getValue() {
		return this.value;
	}

	//evaluates the node
	evaluate() {
		//return the key value
		return this.value;
	}
}

//export
exports.KeyNode = KeyNode;

//end of file
