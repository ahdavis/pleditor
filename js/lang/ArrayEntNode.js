/*
 * ArrayEntNode.js
 * Defines a class that represents an AST node for an array entry
 * Created on 2/20/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
const NodeType = require('./NodeType.js');
const ASTNode = require('./ASTNode.js').ASTNode;

//class definition
class ArrayEntNode extends ASTNode {
	//constructor
	constructor(value, next) {
		//call the superclass constructor
		super(NodeType.N_ARRENT, value, next);
	}

	//evaluates the node
	evaluate() {
		//get the array entry value
		var value = this.left.evaluate();

		//get whether this entry is the last entry
		var isNotLast = (this.right !== null);

		//append the rest of the array
		//if this entry is not the last entry
		if(isNotLast) {
			value += ', ';
			value += this.right.evaluate();
		}

		//and return the value
		return value;
	}
};

//export
exports.ArrayEntNode = ArrayEntNode;

//end of file
