/*
 * TokenType.js
 * Defines token types for plist parser tokens
 * Created on 2/20/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//no imports

//constant definitions
const T_LBRACE = 0; //left brace
const T_RBRACE = 1; //right brace
const T_LPAREN = 2; //left parenthesis
const T_RPAREN = 3; //right parenthesis
const T_NUM = 4; //number
const T_STR = 5; //string
const T_EQU = 6; //equals sign
const T_COMMA = 7; //comma
const T_KEY = 8; //dictionary key
const T_SEMIC = 9; //semicolon
const T_EOT = 10; //end of input text

//exports
exports.T_LBRACE = T_LBRACE;
exports.T_RBRACE = T_RBRACE;
exports.T_RPAREN = T_RPAREN;
exports.T_LPAREN = T_LPAREN;
exports.T_NUM = T_NUM;
exports.T_STR = T_STR;
exports.T_EQU = T_EQU;
exports.T_COMMA = T_COMMA;
exports.T_KEY = T_KEY;
exports.T_SEMIC = T_SEMIC;
exports.T_EOT = T_EOT;

//end of file
